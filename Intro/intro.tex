% Activate the following line by filling in the right side. If for example the name of the root file is Main.tex, write
% "...root = Main.tex" if the chapter file is in the same directory, and "...root = ../Main.tex" if the chapter is in a subdirectory.
 
%!TEX root =  ../main.tex

\chapter*{Introduction}
  \addcontentsline{toc}{chapter}{Introduction}

% Define problem of quantization
% Quantization problem as the approximation of measures by finitely supported ones
The problem of quantization involves the approximation of finite measures in a metric space by measures supported on finitely many points. This problem has been investigated in various equivalent formulations for applications in fields such as signal processing, statistical data analysis, numerical integration, or financial mathematics.
The purpose of this thesis is to provide a broad, mathematically rigorous treatment of the quantization problem from a measure theoretic perspective, with few assumptions on the structure of the underlying domain.

% [History, fundamental results, formulae, literature]
\paragraph{Historical overview.}
The quantization problem was first introduced in the field of signal processing for measures on the real line. This problem is now also referred to as \emph{scalar quantization}, in contrast to \emph{vector quantization} on $\RR^d$ and \emph{functional quantization} on infinite-dimensional vector spaces. In this original context, a \emph{quantizer} is a map $T \colon \RR \to \RR$ whose image $T(\RR)$ is finite or discrete, and is chosen in order to minimize a certain distortion measure among quantizers of a desired degree of granularity, such as quantizers for which $T(\RR)$ has at most $n$ elements for some fixed natural number $n$.  A common class of distortion measures is the mean $L^p$ error with respect to a probability density $\rho$ on $\RR$, given by
\[ \int_{-\infty}^\infty |x-T(x)|^p \rho(x) \de x. \]
A list of fundamental results concerning the quantization problem, such as the existence, uniqueness and characterization of optimal quantizers, algorithms for the construction of optimal or otherwise efficient quantizers, and asymptotic bounds for the quantization error in the limit of large $n$, have first been obtained for the case of the real line. % [References]
The well-known \emph{Lloyd's method}, which is a general iterative algorithm for the construction of quantizers, was first introduced by Lloyd \cite{lloyd} in this context of scalar quantization with $p = 2$. Later algorithms have also been devised for the specific setting of the real line, as well as more general settings. 
More general cost functions, in place of $|\cdot|^p$, have also been considered by Kieffer \cite{kief82, kieffer}, who proved the existence of optimal quantizers and also established conditions for their uniqueness as fixed points of Lloyd's method.

The asymptotic behavior of the quantization error in the limit of large $n$ was first investigated for the case $p = 2$ by Bennett \cite{bennett} and independently by Panter and Dite \cite{pandite}, who established the decay of the quantization error to zero on the order of $n^{-2}$ for absolutely continuous probability distributions with bounded support. An extension of this result to unbounded probability measures on the real line with arbitrary $p < \infty$, satisfying the moment condition $\int |x|^{p+\delta} \rho(x) \de x < \infty$ for some $\delta > 0$, was later provided by Pierce \cite{pierce}.
% [Asymptotic bounds first by Bennett for $p = 2$]
% [More history, references, Lloyd's method, $k$-means etc., gradient flows]

Most of the properties of scalar quantization have also been generalized to the setting of $\RR^d$, equipped with a norm, in which however the task of finding optimal solutions is made more difficult by the nonconvexity of the problem. 
For the exponent $p = 1$, this problem is also considered in economic planning under the name of \emph{location problem}, as presented e.g. in Bourne and Roper \cite{bourne}.  % Connection to optimal transport?
General results of existence, stability and characterization of optimal quantizers in this setting have been presented by Gersho and Gray \cite{gersho92vector} and Graf and Luschgy \cite{quantbook}. % More?

Asymptotic estimates for the quantization error have also been obtained for vector quantization, as a generalization for the results on the real line.
The fundamental \emph{Zador theorem} for the asymptotic behavior of the quantization error, given in its most general form by Graf and Luschgy \cite[Thm. 6.2]{quantbook}, states that for probability measures $P$ on $\RR^d$ satisfying the moment condition $\int \|x\|^{p+\delta} \de P(x) < \infty$
for some $\delta > 0$, the following asymptotic estimate holds:
\[ \lim_{n\to\infty} n^{p/d} V_{n,p}(P) = Q_p([0,1]^d) \left( \int h^{\frac{d}{d+p}} \de x \right)^{\frac{d+p}{d}}, \]
where $V_{n,p}(P)$ is the minimal value of the quantization error $\int \|x - T(x)\|^p \de P(x)$ over quantizers for which $|T(\RR^d)| \leq n$, $h$ is the density of the absolutely continuous component of $P$, and $Q_p([0,1]^d)$ is called the \emph{quantization coefficient of the unit cube}. This result also holds if $P$ is singular, in which case the right-hand side is zero, and even when the moment condition is not satisfied, the right-hand side provides a lower bound for $\liminf_{n\to\infty} n^{p/d} V_{n,p}(P)$. 

% $p = \infty$ case
The quantization problem also admits an extension to the limiting case $p = \infty$, albeit with a different expression for the quantization cost. The problem in this case is called the \emph{covering problem} and is purely geometrical in nature: it concerns the smallest radius $r$ for which the support of a compactly supported measure, or more generally a bounded set, can be covered with $n$ closed balls of radius $r$. This problem has also been investigated in Graf and Luschgy \cite[Sect. 10]{quantbook}, following prior sources in convex geometry unrelated to quantization.

This overview of the fundamental results in scalar and vector quantization is by no means comprehensive. For further reference, consult the survey by Gray and Neuhoff \cite{grayneuhoff} and the book by Gersho and Gray \cite{gersho92vector} for a more detailed history of quantization in signal processing, and Pag\`es \cite{pagesintro} for a more recent survey of vector quantization and its applications.

\paragraph{Recent developments.}

The quantization problem has also been considered in the setting of irregular fractal domains, such as \emph{self-similar sets}.
A set (resp. measure) is called self-similar if it can be expressed as a union (resp. convex combination) of images (resp. pushforwards) of itself under a finite collection of contractions, which are usually also required to be affine maps. These sets of contractions, called \emph{iterated function systems} (IFS), in fact characterize self-similar sets uniquely. Common examples of self-similar sets are the dyadic Cantor set on the interval $[0,1]$, the Sierpinski triangle, and also the cube $[0,1]^d$. 

Self-similar sets, and the Cantor set in particular, have been investigated extensively in the context of the quantization problem beginning with Zador \cite{zador}, who proved a variant of Bennett's formula for the Cantor set and observed that the exponent in this case coincided with the fractal dimension of the Cantor set. The same connection was also indicated by Graf and Luschgy \cite{Graf-1997}, who also provided an explicit formula for the quantization error for fixed $n$ and characterized all optimal sets of centers. The subsequent book by Graf and Luschgy \cite[Sect. 14]{quantbook} proved novel estimates for general self-similar sets in $\RR^d$ satisfying a certain separation condition, which they have further relaxed in \cite{Graf-2002}. We have also encountered a recent preprint by Roychowdhury and Verma \cite{roychowdhury2020study}, which extends the results of Graf and Luschgy to more general metric spaces, with less constraints on the contractions constituting the IFS.

Asymptotic estimates for the quantization error have also been obtained for the broader class of \emph{Ahlfors regular} sets and measures by Graf and Luschgy \cite[Sect. 12]{quantbook}, with a simpler argument later provided by Kloeckner \cite[Sect. 4]{discapprox}. 
Another class of singular measures investigated for the purpose of quantization is that of measures supported on \emph{rectifiable curves}.
Rectifiable curves are compact, non-intersecting continuous paths of finite length and are rudimentary examples of \emph{rectifiable sets} as treated in geometric measure theory. 
Graf and Luschgy \cite[Sect. 13]{quantbook} showed that on $\RR^d$ equipped with the usual Euclidean norm, the quantization coefficients of length measures for rectifiable curves admit the same expression as those of uniform measures on intervals on $\RR$. 

In the same section, Graf and Luschgy also conjectured that such a result would hold more generally for rectifiable sets and measures of arbitrary dimension. While not explicitly resolved, this conjecture has been answered in the positive for the important special class of Riemannian manifolds, considered intrinsically rather than as embedded submanifolds of $\RR^d$. 
Kloeckner \cite[Sect. 5]{discapprox} extended the Zador theorem to compactly supported measures on Riemannian manifolds, later generalized by Iacobelli \cite{iacasym} to the non-compact case with an analogous moment condition. 
This correspondence suggests a type of invariance of domain regarding the asymptotic performance of quantization and presents a possible avenue for further research.

% [Further domains, functional quantization, manifolds, non-compact case]

Alternative cost functions in place of the $L^p$ distance, and the associated \emph{$L^p$ Wasserstein metric} on probability measures, have also been considered in the quantization problem. Graf and Luschgy \cite{Graf-2009} have investigated the quantization problem with respect to the \emph{Prokhorov metric} for probability measures on $\RR^d$, proving similar results for the existence of optimal solutions and asymptotic bounds for the rate in which the quantization error converges to $0$. % Further cost functions have been introduced in signal processing and image processing literature, for which the integrand in the quantization cost depends separately on the point $x$ and the difference $x - T(x)$, most commonly in the form $(x-T(x))^\top B(x) (x - T(x))$ where $B(x)$ is a position-dependent positive definite matrix. Such error measures are called \emph{input-weighted} error measures, and are treated e.g. in Delattre et al. \cite{Delattre2004} 
Other integral-based cost functions, for which the integrand is a more general function of the distance $\|x - T(x)\|$ than the $p$th power, are treated in Delattre et al. \cite{Delattre-2004}.
Lastly, the quantization problem has also been investigated on infinite-dimensional domains, with respect to the quantization of stochastic processes viewed as measures on Hilbert or Banach spaces, by Luschgy and Pag\`es \cite{LUSCHGY2002486} and by Graf et al. \cite{Graf-2007}.


% logarithm of distance
% entropy instead of fixed n

% Applicability to multiple domains

% [State of the art, new results, applicability to different domains]

\paragraph{Literature and references.}

% Literature
% Graf-Luschgy book, Vector quantization book, Gray-Neuhoff survey
% Quantization dimension introduced by Zador
% [Main resource Graf and Luschgy considering quantization on $\RR^d$ with a norm, further resources specific to vector quantization, later articles]

The most inclusive overview of the quantization problem in literature, covering the general properties of the quantization problem as well as asymptotics for nonsingular and singular measures on $\RR^d$, seems to be the book by Graf and Luschgy \cite{quantbook}, which is also the main reference the thesis will follow. The book is divided into three chapters, which cover respectively the general properties of quantization, the asymptotics of quantization for nonsingular measures, and asymptotics for singular measures, the latter being largely novel results. The book by Gersho and Gray \cite{gersho92vector} provides a more detailed treatment of vector quantization from the perspective of signal processing. % Lack of any other book

In this thesis, we will largely follow the book by Graf and Luschgy in our treatment of the quantization problem, with additional reference to the more contemporary articles cited above, especially Kloeckner \cite{discapprox}. For preliminary material on measure theory, optimal transport, metrics on probability measures, and the dimension theory of fractals, we will also consult the books by Billingsley \cite{billing}, Dudley \cite{dudley}, Villani \cite{villani, villanioton}, Ambrosio et al. \cite{ambrosio}, Santambrogio \cite{otam}, Bogachev \cite{bogachev} and Falconer \cite{falc97, falc}, among other references listed in the beginning of each chapter.

% State of the art
% More recent research has applied quantization to infinite-dimensional domains, gradient flows etc., complete metric spaces, Kloeckner

\subsection*{Outline, original contributions and further directions}

The thesis consists of six chapters, which can be viewed in three groups of two. 
The first two chapters recall preliminary material regarding finite measures on metric spaces and common distances applied to measures. 
The third and fourth chapters detail the properties of the quantization problem for fixed $n$, respectively in general domains and in the special setting of $\RR^d$. 
The latter two chapters cover the asymptotics of quantization, presented now in a general formalism with new estimates for basic operations, such as combinations of measures and perturbations of small mass, which are afterwards applied in specific domains to recover and extend the previous results for nonsingular and singular measures.
We also provide an appendix, which recalls the properties of probability measures on $\RR$, such as equivalent characterizations in terms of cumulative distribution functions or quantile functions.

% Overview of the thesis, including formulas and references
We will begin the thesis with a preliminary review of the properties of measures on metric spaces and the basic principles of optimal transport. The next chapter will introduce a common class of metrics on probability measures derived from optimal transport, namely the $L^p$ \emph{Wasserstein} (or \emph{Monge-Kantorovich}) \emph{metrics}, with respect to which the quantization problem is considered.
This presentation is intended not only to provide a list of prerequisite definitions and results, but also to demonstrate why these metrics provide a natural and desirable setting in which to consider the approximation of measures.
While the results in these two chapters are established, they have been gathered from a disparate collection of references; the first two chapters hence also provide a standalone survey of these results.

The quantization problem proper will be introduced in Chapter \ref{ch:quantprob}, in a general metric space setting. The chapter will present the different equivalent formulations of the problem, in terms of optimal transport, optimal partitions and optimal sets of centers, and treat basic questions of optimization regarding the problem, namely necessary conditions for a solution to be optimal, the existence of optimal solutions for a given measure, and the stability of optimal solutions with respect to perturbations of the measure. The chapter consists largely of results given in Sections 3, 4 and 10 of Graf and Luschgy \cite{quantbook}, reordered for clarity and extended to the setting of general complete and separable metric spaces. % More precise definitions?

A further list of properties associated with scalar and vector quantization will be presented in Chapter \ref{ch:quantrd}. The material in this chapter follows Sections 1, 2 and 4 of Graf and Luschgy \cite{quantbook} closely, and includes the results that make specific use of the linear or norm structure on $\RR^d$ and hence have not been covered in previous chapters. These results consist of geometric properties arising from the strict convexity or smoothness of the underlying norm, the particular formulation of the problem in one dimension including conditions for the uniqueness of optimal solutions, and upper and lower bounds for the quantization error. While the former is largely self-contained, the latter bounds will be used in Chapter \ref{ch:quantcoeffs} in reference to the asymptotics of quantization for measures on $\RR^d$.

The remainder of the thesis will be concerned with the asymptotics of the quantization error. Chapter \ref{ch:quantasym} will introduce two useful metrics, the upper and lower \emph{quantization dimensions} and \emph{quantization coefficients}, that measure the rate of decay with which the quantization error converges to zero as the number of points approaches infinity. These notions will turn out to be related to more common notions of fractal dimension, namely the \emph{box-counting dimension}, the \emph{Hausdorff dimension} and \emph{Ahlfors regularity}. Section \ref{sect:dims} will first provide some background on these notions of dimension, mostly following Falconer \cite{falc97, falc} and Hutchinson \cite{hutch}. 

% Possibly give more precise definitions here?
Section \ref{sect:quantdim} will then introduce the quantization dimension and coefficients. Basic definitions and properties, as well as the relation to prior notions of fractal dimension, will largely follow Graf and Luschgy \cite[Sect. 11]{quantbook}, with a more direct argument for Ahlfors regularity adapted from Kloeckner \cite{discapprox}. 
The remainder of the chapter will consist of original results regarding the behavior of the quantization dimension and coefficients with respect to operations on measures, such as combinations or total variation convergence, and the asymptotic behavior and spatial distribution of asymptotically optimal sequences of quantizers. 
These results have been generalized from arguments given in Graf and Luschgy \cite[Sect. 6]{quantbook} and Kloeckner \cite{discapprox} for absolutely continuous measures on $\RR^d$, but apply under much more general conditions. 

This is facilitated in particular by an extension of the \emph{localization} property given in Kloeckner \cite[Lem. 3.5]{discapprox}, which states that optimal solutions to the quantization problem will also provide dense coverings of the support of the measure in the limit of large numbers of centers. This result allows us to establish the continuity of quantization coefficients with respect to total variation convergence of measures, bounds and explicit expressions for the quantization coefficients of sums of measures in terms of those of the component measures, and additional invariance properties under embeddings and changes of metric. In future research, these properties might be further extended and applied to sharpen the asymptotic bounds given for Ahlfors regular measures. We also introduce a further notion derived from the quantization coefficients, referred to as the \emph{quantization content} associated to an underlying measure, which encodes the asymptotic distribution of sets of centers in space, and use it to extend the \emph{equidistribution} property described in Kloeckner \cite[Sect. 7]{discapprox} for compactly supported measures on $\RR^d$. % Delattre also
% Consequences: invariance of domain, precise calculations, further research

Chapter \ref{ch:quantcoeffs} finally covers the computation of quantization coefficients for specific classes of measures. The results in this chapter are largely established, but the general formalism of the previous chapter allows us to provide simpler statements and proofs. % more?
In Section \ref{sect:quantasymrd}, we consider the class of measures on $\RR^d$, and prove the Zador theorem in its general form given by Graf and Luschgy \cite[Thm. 6.2]{quantbook} as an application of the estimates developed in the previous chapter. Using localization and its implications on invariance of domain, it might be possible to extend this argument further to recover the results obtained by Kloeckner \cite{discapprox} and Iacobelli \cite{iacasym} for Riemannian manifolds. We will also provide a further list of properties satisfied by optimal quantizers, again as a consequence of the general results associated with the quantization content.

Paralleling Sections 13 and 14 of Graf and Luschgy \cite{quantbook}, we also present more specific estimates for two classes of singular measures, namely length measures on \emph{rectifiable curves} and \emph{self-similar measures}, now stated in the context of more general metric spaces. 
In Section \ref{sect:quantcurve}, using the previously developed results for combinations of measures, we present a new argument for the quantization of rectifiable curves which only makes use of the metric structure, in place of a less direct argument given in Graf and Luschgy \cite[Lem. 13.11]{quantbook} which involves orthogonal projections. 
This will allow for the statements presented to be generalized readily to rectifiable curves on arbitrary metric spaces. 
In future research, it might be possible to apply the results of Section \ref{sect:quantdim} to the asymptotic quantization of more general rectifiable sets, but with arguments different from those given for rectifiable curves.

Section \ref{sect:quantselfsim} will finally present the key results concerning the asymptotic quantization of self-similar sets and measures. 
After recalling a collection of known statements regarding the dimensionality of self-similar sets, we will apply the non-asymptotic estimates of Section \ref{sect:quantdim} to provide more succinct proofs of the results obtained by Graf and Luschgy \cite{Graf-2002} for the quantization coefficients of self-similar sets and measures, adapting the arguments provided later by Roychowdhury and Verma \cite{roychowdhury2020study}.

% Original contributions

% Conclusions, further research

% Notation, references
